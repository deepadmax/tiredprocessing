import time
from multiprocessing import Process
from multiprocessing.sharedctypes import Synchronized
from threading import Thread


class SleepyProcess:
    """Wrapper around `multiprocessing.Process` that deals with the process' termination."""

    INTERMITTENT_CHECK_DELAY = 0.02

    def __init__(self, *_args, **_kwargs):
        self.process = Process(*_args, **_kwargs)

    def start(self, status: Synchronized = None):
        self.process.start()
        Thread(target=self._stop, kwargs=dict(status=status)).start()

    def _stop(self, status: Synchronized = None):
        try:
            while self.process.is_alive():
                time.sleep(self.INTERMITTENT_CHECK_DELAY)

                if status is not None and status.value is not True:
                    break
        finally:
            self.process.join()